module.exports = {
  devServer: {
    port: 80
  },
  productionSourceMap: false,
  lintOnSave:false,
}
